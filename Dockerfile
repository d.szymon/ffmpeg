FROM nvidia/cuda:12.4.1-devel-ubuntu22.04 as builder

ENV DEBIAN_FRONTEND noninteractive

# number of CPU's use for compilation
ARG CPUS=8

ENV NVIDIA_DRIVER_CAPABILITIES video,compute,utility

# install deps
RUN apt-get update && apt-get -y install \
    autoconf \
    automake \
    build-essential \
    cmake \
    git-core \
    libass-dev \
    libfreetype6-dev \
    libgnutls28-dev \
    libmp3lame-dev \
    libtool \
    libvorbis-dev \
    meson \
    ninja-build \
    pkg-config \
    texinfo \
    wget \
    yasm \
    zlib1g-dev \
    libunistring-dev \
    libaom-dev \
    libdav1d-dev \
    libssl-dev \
    nasm \
    libvpx-dev \
    libopus-dev

WORKDIR /app

RUN mkdir -p ffmpeg_sources bin ffmpeg_build

# install ffmpeg-nvidia adapter
RUN mkdir ~/nv && cd ~/nv && \
  git clone https://github.com/FFmpeg/nv-codec-headers.git && \
  cd nv-codec-headers && make install

# compile ffmpeg with cuda
RUN cd ~/nv && \
  git clone https://git.ffmpeg.org/ffmpeg.git ffmpeg/ && \
  cd ffmpeg && \
  PKG_CONFIG_PATH="/app/ffmpeg_build/lib/pkgconfig" ./configure \
    --prefix="/app/ffmpeg_build" \
    --enable-nonfree \
    --enable-gpl \
    --enable-cuda-nvcc \
    --enable-libnpp \
    --extra-cflags="-I/usr/local/cuda/include -I/app/ffmpeg_build/include" \
    --extra-ldflags="-L/usr/local/cuda/lib64 -L/app/ffmpeg_build/lib" \
    --extra-libs="-lpthread -lm" \
    --ld="g++" \
    --disable-static \
    --enable-openssl \
    --disable-ffplay \
    --enable-libopus \
    --enable-shared \
    --enable-libvpx \
    --enable-libopus \
    --disable-doc \
    --disable-network \
    && \
  make -j $CPUS && \
  make install

FROM nvidia/cuda:12.4.1-runtime-ubuntu22.04
ENV NVIDIA_DRIVER_CAPABILITIES video,compute,utility
RUN apt-get update && apt-get -y install \
    libssl-dev \
    nasm \
    libvpx-dev \
    libopus-dev \
  && rm -rf /var/lib/apt/lists/* 
COPY --from=builder /app/ffmpeg_build/bin /bin
COPY --from=builder /app/ffmpeg_build/lib /lib
